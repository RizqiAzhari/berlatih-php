<?php
function tukar_besar_kecil($string){
    //kode di sini
    for ($i = 0; $i < strlen($string); $i++){
        //ctype_upper() untuk mengecek kondisi true apabila hurufnya besar
        if (ctype_upper($string[$i])){
            //strtolower() untuk menjadikan string huruf kecil
            $new_string[$i] = strtolower($string[$i]);
        } else {
            //strtolower() untuk menjadikan string huruf besar
            $new_string[$i] = strtoupper($string[$i]);
        }
    }
    //implode() untuk konversi array ke string
    return implode($new_string).'<br>';
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>